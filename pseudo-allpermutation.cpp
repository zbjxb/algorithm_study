#include <iostream>

using namespace std;

void sort(char* perm, int from, int to) {
  for (int n=from;n<to;++n) {
    char& min=perm[n];
    for (int i=n+1;i<=to;++i) {
      if (min > perm[i]) {
	std::swap(min, perm[i]);
      }
    }
  }
}

void permutation(const char* cperm, int cfrom, int cto, char* perm, int from, int to)
{
  if (from > to) {
    for (int i=cfrom; i<=cto; ++i) {
      cout << perm[i];
    }
    cout << endl;
  }
  else {
    for (int j=cfrom; j<=cto; ++j) {
      perm[from] = cperm[j];
      permutation(cperm, cfrom, cto, perm, from+1, to);
    }
  }
}

void pseudo_allpermutation(char* perm, int from, int to) {
  if (from > to) {
    return;
  }
  else if (to==from) {
    cout << perm[from] << endl;
    return;
  }

  sort(perm, from, to);
  char* cperm = new char[to-from+1];
  memcpy(cperm, perm, to-from+1);

  permutation(cperm, from, to, perm, from, to);

  delete [] cperm;
}

int main()
{
  int index=0;
  char str[1024];

  char ch = getchar();
  while (ch != EOF)
  {
    if (ch == '\n')
    {
      str[index]=0;
      pseudo_allpermutation(str, 0, index-1);
      index=0;
    }
    else
    {
      str[index++] = ch;
    }
    ch = getchar();
  }
  return 0;
}

