#include <stdio.h>
#include <ctype.h>

void reverse_string(char* s, int from, int to)
{
  while (from < to)
  {
    char c=s[from];
    s[from++]=s[to];
    s[to--]=c;
  }
}

void parseSentence(char* str, int index)
{
  int start, end;

  for (int i=0;i<index;i++)
  {
    if (isspace(str[i]))
    {
      continue;
    }

    start = i;
    for (i++;i<index;i++)
    {
      if (isspace(str[i]))
      {
        break;
      }
    }
    end=i-1;

    reverse_string(str, start, end);
  }
  
  reverse_string(str, 0, index-1);
}

int main(int argc, char** argv)
{
  int index=0;
  char str[128]={0};

  char ch=getchar();
  while (ch != EOF)
  {
    if (ch=='\n')
    {
      parseSentence(str, index);
      str[index]=0;
      printf(str);
      printf("\n");
      index=0;
    }
    else
    {
      str[index++]=ch;
    }
    ch=getchar();
  }
  return 0;
}


