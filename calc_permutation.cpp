#include <iostream>
#include <string.h>
#include <algorithm>

void calc_permutation(char* perm, int from, int to)
{	
  if (from == to)
  {
    printf(perm);
    printf("\n");
  }
  else
  {
    for (int i=from;i<=to;++i)
    {
      std::swap(perm[from],perm[i]);
      calc_permutation(perm, from+1, to);
      std::swap(perm[from],perm[i]);
    }
  }
}

int main()
{
  char str[]="abc";
  calc_permutation(str, 0, strlen(str)-1);
  getchar();
  return 0;
}
