#include <stdio.h>
#include <string.h>

void rotate(int nums[], int n, int k)
{
  int* p=new int[n];
  memcpy(p, nums, n*sizeof(int));
  memcpy(nums, p+n-k, k*sizeof(int));
  memcpy(nums+k, p, (n-k)*sizeof(int));
  delete [] p;
}

int main()
{
  int nums[]={1,2,3,4,5,6,7};
  rotate(nums, 7, 6);
  return 0;
}

