#include <iostream>
using namespace std;

bool my_next_permutation(char* perm, int num)
{
  int i;
  for (i=num-2;i>=0&&perm[i]>=perm[i+1];--i)
    ;
  if (i<0) {
    return false; // this is alreay the greatest permutation
  }
  
  int j;
  for (j=num-1;j>i&&perm[i]>=perm[j];--j)
    ;

  swap(perm[i], perm[j]);
  reverse(perm+i+1, perm+num);
  return true;
}

int main()
{
  int index=0;
  char str[1024];

  char ch = getchar();
  while (ch != EOF)
  {
    if (ch == '\n')
    {
      str[index]=0;
      if (my_next_permutation(str, index)) {
	cout << str;
      }
      index=0;
    }
    else
    {
      str[index++] = ch;
    }
    ch = getchar();
  }
  return 0;
}
